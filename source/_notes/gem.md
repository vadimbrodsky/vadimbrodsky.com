---
layout: note
title: RubyGems
subhead: Ruby package manager.
---

## Gems Version
- To find out which version of Ruby Gems is installed.

```bash
gem -v
which gem
```

## Install a Gem
- To install a specific version pass the `-v <version>` flag.

```bash
gem install rails
gem install rails -v 4.0.8
```


## Installed Gems
- To find out which gems are installed.

```bash
gem list
```


## Update the Installed Gems
- Go to the server and update the local gems.
- Or to update the Gems itself.

```bash
gem update
```

```bash
gem update --system
```
