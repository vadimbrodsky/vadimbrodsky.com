---
layout: post
status: publish
published: true
title: Responsive Email Patterns
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 138
wordpress_url: http://www.vadimbrodsky.com/?p=138
date: '2015-05-07 10:01:05 -0400'
date_gmt: '2015-05-07 14:01:05 -0400'
categories:
- Uncategorized
tags: []
---
<blockquote>Responsive Email PatternsA collection of patterns &amp; modules for responsive emails</blockquote></p>
<p>Source: <em><a href="http://responsiveemailpatterns.com/">Responsive Email Patterns</a></em></p>
