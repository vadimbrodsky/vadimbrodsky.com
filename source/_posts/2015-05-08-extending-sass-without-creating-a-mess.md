---
layout: post
status: publish
published: true
title: Extending Sass Without Creating A Mess
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 150
wordpress_url: http://www.vadimbrodsky.com/?p=150
date: '2015-05-08 09:28:40 -0400'
date_gmt: '2015-05-08 13:28:40 -0400'
categories:
- CSS / Sass
tags: []
---
<blockquote>The @extend directive in Sass is a powerful directive that facilitates the sharing of rules and relationships between selectors. However, it can produce undesirable side effects if it is not carefully implemented. Thankfully, there are many strategies for using @extend effectively that can prevent these side effects and produce clean, organized CSS.</blockquote></p>
<p>Good read.</p>
<p>Source: <em><a href="http://www.smashingmagazine.com/2015/05/04/extending-in-sass-without-mess/">Extending Sass Without Creating A Mess</a></em></p>
