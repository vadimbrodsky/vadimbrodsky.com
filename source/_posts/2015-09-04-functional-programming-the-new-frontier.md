---
layout: post
title: Functional Programming
subhead: The New Frontier?
date: '2015-09-04 23:42:51 -0400'
date_gmt: '2015-09-05 03:42:51 -0400'
categories:
- Programming
tags:
- functional programming
- elm
- elixir
---

Functional programming languages seem to gain resurgence in popularity recently, languages like [Elixir](http://elixir-lang.org/), [Elm](http://elm-lang.org/), [Clojure](http://clojure.org/), [Scala](http://www.scala-lang.org/) and [Haskell](https://www.haskell.org/) are gaining mind share in web development.

Is it another programming fad that will come and go? Maybe, but some very wise people are talking about Functional languages and they raise good points.

- Moore's Law is ~~stagnating~~ failing
- Multi-core architectures are promoted over CPU clock speed
- Concurrency is hard on OOP languages

[Robert C. Martin (Uncle Bob)](https://twitter.com/unclebobmartin) has a great presentation on the subject, see below. [Dave Thomas](https://twitter.com/pragdave) strongly advised learning Functional Programming on the [Code Newbie Podcast](http://www.codenewbie.org/podcast/the-pragmatic-programmer-part-ii) (@ 1h 13 minutes).

{% youtube "https://www.youtube.com/embed/7Zlp9rKHGD4" %}

I have been curious about Function Programming for a while now, and it sounds like now is a good time to approach the subject.
