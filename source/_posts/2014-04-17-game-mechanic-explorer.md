---
layout: link
title: 'Game Mechanic Explorer'
link: 'http://gamemechanicexplorer.com'
category: link
tags: ['js', 'algorithm', 'game development', 'javascript']
---

Cool collection of game mechanics, algorithms and effects by John Watson. All written in JavaScript and playable in the browser.

> A collection of concrete examples for various game mechanics, algorithms, and effects.

via -- [gamemechanicexplorer.com](http://gamemechanicexplorer.com)