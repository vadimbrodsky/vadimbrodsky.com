---
layout: post
status: publish
published: true
title: StackEdit
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 75
wordpress_url: http://www.vadimbrodsky.com/?p=75
date: '2015-02-07 09:00:33 -0500'
date_gmt: '2015-02-07 14:00:33 -0500'
categories:
- JavaScript
tags:
- GitHub
- tool
- web app
- markdown
- editor
---
<blockquote>
  <a href="https://stackedit.io/">StackEdit</a> is a full-featured, open-source Markdown editor based on PageDown<br />
</blockquote></p>
<p>In the age of the niche <a href="https://itunes.apple.com/us/app/ia-writer-pro/id775737590?mt=12&amp;ign-mpt=uo%3D4">market</a> of distraction free MarkDown editors StackEdit is surprisingly full featured and pleasant to use.</p>
<p>You can event deploy it to <a href="https://github.com/benweet/stackedit/blob/master/doc/developer-guide.md#deploy">your own Heroku instance</a>.</p>
<p>via -- <a href="https://github.com/benweet/stackedit/">SlackEdit by Benoit Schweblin</a></p>
