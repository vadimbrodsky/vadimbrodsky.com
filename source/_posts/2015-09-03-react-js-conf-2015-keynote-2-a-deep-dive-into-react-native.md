---
layout: post
title: React.js Conf 2015 Keynote 2
subhead: A Deep Dive into React Native
date: '2015-09-03 18:37:37 -0400'
categories:
- JavaScript
tags:
- react
- conference
---

{%youtube "https://www.youtube.com/watch?v=7rDsRXj9-cU" %}

This looks like it's more powerful than PhoneGap / Cordova.

via – [React.js Conf 2015 Keynote 2 - A Deep Dive into React Native - YouTube](https://www.youtube.com/watch?v=7rDsRXj9-cU).
