---
layout: post
title: The Lord of the Rings
date: '2015-02-11 22:41:27 -0500'
categories:
- Books
tags:
- Fantasy
- Videos
- Motion Graphics
---

I have read the [Silmarillion](http://en.wikipedia.org/wiki/The_Silmarillion) and the [LOTR Trilogy](http://en.wikipedia.org/wiki/The_Lord_of_the_Rings) long time ago, and my plan for 2015 is to reread these books again. Thanks to the great work of [C.G.P Grey](https://www.youtube.com/user/CGPGrey/) explaining complex things, I am inspired to to start this soon*.

In the meanwhile here are two videos to get the lore knowledge up to speed.

{% youtube "https://www.youtube.com/watch?v=YxgsxaFWWHQ" %}

{% youtube "https://www.youtube.com/watch?v=WKU0qDpu3AM" %}
