---
layout: post
title: Building an Ember.js Application - YouTube
date: '2015-02-20 14:20:03 -0500'
categories:
- JavaScript
tags:
- web development
- ember.js
- tutorial
---

{% youtube "https://www.youtube.com/watch?v=1QHrlFlaXdI" %}

This looks really slick, it's amazing how much can be achieved when embracing convention over configuration. Looking to dive deeper into [Ember.js](http://emberjs.com/).

[Building an Ember.js Application - YouTube](https://www.youtube.com/watch?v=1QHrlFlaXdI).
