---
layout: post
status: publish
published: true
title: Colors - A nicer color palette for the web.
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 157
wordpress_url: http://www.vadimbrodsky.com/?p=157
date: '2015-07-20 11:27:17 -0400'
date_gmt: '2015-07-20 15:27:17 -0400'
categories:
- CSS / Sass
tags:
- colours
---
<blockquote>COLORS A nicer color palette for the web.</blockquote></p>
<blockquote><p>The New Defaults<br />
Skinning your prototypes just got easier - colors.css is a collection of skin classes to use while prototyping in the browser.</blockquote></p>
<blockquote><p>647 Bytes minified and gzipped.</blockquote></p>
<p>I wish these were the default in all browsers.</p>
<p>Source: <em><a href="http://clrs.cc/">Colors - A nicer color palette for the web.</a></em></p>
