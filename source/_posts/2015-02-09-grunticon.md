---
layout: post
status: publish
published: true
title: Grunticon
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 97
wordpress_url: http://www.vadimbrodsky.com/?p=97
date: '2015-02-09 17:01:13 -0500'
date_gmt: '2015-02-09 22:01:13 -0500'
categories:
- JavaScript
tags:
- web development
- lib
---
<blockquote>
  Grunticon is a Grunt.js task that makes it easy to use SVGs (Scalable Vector Graphics) for crisp, resolution independent icons, logos, illustrations and background images. SVGs aren't supported everywhere so Grunticon auto-generates bitmap (PNG) fallback images and loads the right format for compatibility with pretty much any browser.<br />
</blockquote></p>
<p>Looks like two colour icons will soon be a thing of the past on the web. Vector graphics is defiantly the best way to create assets for Retina screens.</p>
<p>via -- <a href='http://www.grunticon.com/'>Grunticon</a>.</p>
