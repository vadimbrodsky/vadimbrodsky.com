---
layout: post
title: 'Acqui-hire economics'
category: quote
tags: ['economics', 'm&a', 'startups']
---

> The rule of thumb is that only one or two out of every ten startups will succeed as a business. Only one out of a hundred will be a real home run. With the rest, the ideas like Color that seem great on paper but fail to catch on in the real world, an acqui-hire is often the best solution for all parties.

It seems weird compared to any other industry, but good engineers are a commodity in silicone valley, it’s kind of a win win.

via [Why Apple acquiring Color makes sense in Silicon Valley: the strange logic of the acqui-hire - The Verge](http://www.theverge.com/2012/10/18/3522456/why-apple-acquiring-color-makes-sense-in-silicon-valley-the-strange)