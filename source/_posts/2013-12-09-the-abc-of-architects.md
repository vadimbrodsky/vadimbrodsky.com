---
layout: link
title: 'The ABC of Architects'
link: 'http://vimeo.com/56974716'
category: link
tags: ['animation', 'architecture', 'art', 'design', 'video']
---

<iframe src="//player.vimeo.com/video/56974716" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

Architecture always had a soft place in my heart. This is a remarkable video showcasing the ABC of modern architecture.

via -- [vimeo.com](http://vimeo.com/56974716)

