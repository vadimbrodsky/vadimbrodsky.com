---
layout: post
title: jarednova/timber
date: '2015-05-07 15:21:58 -0400'
categories:
- Uncategorized
tags: []
---

> timber - Plugin to write WordPress themes w Object-Oriented Code and the Twig Template Engine

Ever wished for WordPress to use a tempting language instead of raw PHP, it's possible with the Timber plugin.

Via – [jarednova/timber · GitHub](https://github.com/jarednova/timber/)
