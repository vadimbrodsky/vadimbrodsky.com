---
layout: post
title: 'Life Imitates Art'
category: quote
---

> Life imitates Art far more than Art imitates Life

-- Oscar Wilde

I have recently watched an excellent art documentary — _Exit Through the Gift Shop_. It really provokes thought in me on the definition of art and culture today.