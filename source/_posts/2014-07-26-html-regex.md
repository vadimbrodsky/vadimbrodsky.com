---
layout: post
title: Simple Regular Expression for HTML Tags
categories: snippet
---

A simple regular expression that find HTML tags.

```
<[^>]*>
```

via -- [Stack Overflow](http://stackoverflow.com/a/11229866)