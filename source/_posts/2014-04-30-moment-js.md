---
layout: link
title: 'Moment.js'
link: 'http://momentjs.com'
category: link
tags: ['js', 'library', 'data', 'javascript']
---

This could be useful when dealing with dates.

> Parse, validate, manipulate, and display dates in javascript.
>
> A javascript date library for parsing, validating, manipulating, and formatting dates.

via -- [momentjs.com](http://momentjs.com)