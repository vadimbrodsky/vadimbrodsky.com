---
layout: post
status: publish
published: true
title: Goya
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 70
wordpress_url: http://www.vadimbrodsky.com/?p=70
date: '2015-02-06 19:54:08 -0500'
date_gmt: '2015-02-07 00:54:08 -0500'
categories:
- JavaScript
tags:
- transpile
- tools
- web app
---
<p>This is an impressive example of a complex client-side web app -- Goya, a Pixel Art editor built with ClojureScript and complied to JavaScript.</p>
<p>This trend of transpiling different languages to JavaScript proves the point that JavaScript is the Bytecode of the web. Allowing to use server languages on the client while JavaScript goes to the server. Oh the irony!</p>
<blockquote><p>
  A pixel-art editor built on ClojureScript and Om.<br />
</blockquote></p>
<p>via -- <a href="https://github.com/jackschaedler/goya">Goya - Jack Schaedler Github</a></p>
