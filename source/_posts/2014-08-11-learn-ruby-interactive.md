---
layout: post
title: 'Learn Ruby the Interactive Way'
---

Recently I got interested in Ruby and I have started playing with the language. I was delighted to find out the wealth of inteactive online tutorials for coding, here is a list of the best ones:

1. [Try Ruby](http://tryruby.org/)
2. [Codecademy](http://www.codecademy.com/tracks/ruby)
3. [Code School](https://www.codeschool.com/paths/ruby)
4. [Treehouse](http://teamtreehouse.com/tracks/rails-development)
5. [RubyMonk](https://rubymonk.com/)
6. [Codewars](http://www.codewars.com/)
7. [Ruby Koans](http://koans.heroku.com/en)
8. [Codelearn](http://www.codelearn.org/)
9. [Hackety Hack](http://hackety.com/)

Education as an interactive process really makes sense.