---
layout: post
status: publish
title: phanan/htaccess
date: '2015-02-09 13:51:49 -0500'
categories:
- WordPress
tags: []
---

> A collection of useful .htaccess snippets, all in one place. I decided to create this repo after getting so tired (and bored) with Googling everytime there's a need of forcing www for my new website.

Useful for many many things.

via -- [phanan/htaccess · GitHub](https://github.com/phanan/htaccess).
