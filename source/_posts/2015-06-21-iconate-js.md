---
layout: post
status: publish
published: true
title: iconate.js
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 159
wordpress_url: http://www.vadimbrodsky.com/?p=159
date: '2015-06-21 11:26:13 -0400'
date_gmt: '2015-06-21 15:26:13 -0400'
categories:
- JavaScript
tags:
- animation
---
<blockquote>Transform your icons with trendy animations</blockquote></p>
<p>I like these small animation libraries for the specific task at hand.</p>
<p>Source: <em><a href="http://bitshadow.github.io/iconate/?utm_source=MergeLinks+Weekly+Newsletter&amp;utm_campaign=25ca14aa62-Weekly_14&amp;utm_medium=email&amp;utm_term=0_89e4785ce1-25ca14aa62-280482425">iconate.js</a></em></p>
