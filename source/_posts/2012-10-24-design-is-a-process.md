---
layout: post
title: 'Design is a Process'
category: quote
tags: ['design thinking', 'graphic design', 'process']
---

> You cannot hold a design in your hand. It is not a thing. It is a process. A system. A way of thinking.

-- Bob Gill, Graphic Design as a Second Language