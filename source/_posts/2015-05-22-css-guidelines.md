---
layout: post
status: publish
published: true
title: CSS Guidelines
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 165
wordpress_url: http://www.vadimbrodsky.com/?p=165
date: '2015-05-22 13:22:20 -0400'
date_gmt: '2015-05-22 17:22:20 -0400'
categories:
- CSS / Sass
tags:
- guide
- best practices
---
<blockquote>High-level advice and guidelines for writing sane, manageable, scalable CSS</blockquote></p>
<p>Excellent collection of best practices and advice from <a href="http://csswizardry.com/about/">Harry Roberts</a> of <a href="http://csswizardry.com/about/">CSS Wizardry</a>. Every front-end developer should read it at least a couple times.</p>
<p>Source: <em><a href="http://cssguidelin.es/">CSS Guidelines</a></em></p>
