---
layout: link
title: '.gitignore.io'
link: 'http://www.gitignore.io'
category: link
tags: ['git', 'development', 'webapp']
---

Found this very useful today – the simplest way to create a .gitignore file for your project's git repo.

> Create useful .gitignore files for your project
>
> Hosting 179 Operating System, IDE, and Programming Language .gitignore templates

via -- [gitignore.io](http://www.gitignore.io)