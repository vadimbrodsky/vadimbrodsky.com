---
layout: link
title: 'Pesticide - Faster CSS layout debugging'
link: 'http://pesticide.io'
category: link
tags: ['css', 'debugging', 'tool']
---

A useful tool to debug CSS layouts.

> Cascading Style Sheets can be tricky. Placing an outline on every element can help you figure out what the fuck is going on. This little css module does exactly just that.

via -- [pesticide.io](http://pesticide.io)