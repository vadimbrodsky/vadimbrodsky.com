---
layout: link
title: 'Redesigning with Sass'
link: 'http://css-tricks.com/redesigning-with-sass/'
category: link
tags: ['css', 'development', 'sass', 'web']
---

Good advice from David Walsh on Sass. I like his useful snippets and mistakes sections.

via -- [css-tricks.com](http://css-tricks.com/redesigning-with-sass/)