---
layout: post
title: 'Frustrating Motivations'
category: quote
tags: ['musings', 'startups']
---

> Frustration motivates you in the same way that scratching an itch does: your interest only lasts long enough to build something that’s “good enough”. After that, it’s solved. Why spend more time on it?


Motivation for creation comes from lazyness, frustration or inspiration.

via -- [Don’t just scratch an itch – Whistling in the Dark](http://builtfromsource.com/blog/2012/10/13/dont-just-scratch-an-itch/)