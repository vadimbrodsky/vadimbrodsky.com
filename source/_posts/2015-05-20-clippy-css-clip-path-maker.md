---
layout: post
status: publish
published: true
title: Clippy &mdash; CSS clip-path maker
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 153
wordpress_url: http://www.vadimbrodsky.com/?p=153
date: '2015-05-20 11:21:27 -0400'
date_gmt: '2015-05-20 15:21:27 -0400'
categories:
- CSS / Sass
tags:
- tool
---
<blockquote>The clip-path property allows you to make complex shapes in CSS by clipping an element to a basic shape (circle, ellipse, polygon, or inset), or to an SVG source.CSS Animations and transitions are possible with two or more clip-path shapes with the same number of points.</blockquote></p>
<p>Source: <em><a href="http://bennettfeely.com/clippy/">Clippy &mdash; CSS clip-path maker</a></em></p>
