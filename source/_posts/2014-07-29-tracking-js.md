---
layout: link 
title: 'tracking.js'
link: 'http://trackingjs.com/'
category: link
tags: ['js', 'library', 'computer vision', 'face detection']
---

This is a very impressive example of how much front-end web technologies eveolved in the last few years. Performing computer vision, colour detection and face recognion in JavaScript.

> The tracking.js library brings different computer vision algorithms and techniques into the browser environment. By using modern HTML5 specifications, we enable you to do real-time color tracking, face detection and much more — all that with a lightweight core (~7 KB) and intuitive interface.

via -- [trackingjs.com](http://trackingjs.com/)
