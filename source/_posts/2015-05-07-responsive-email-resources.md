---
layout: post
status: publish
published: true
title: Responsive Email Resources
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 136
wordpress_url: http://www.vadimbrodsky.com/?p=136
date: '2015-05-07 10:00:44 -0400'
date_gmt: '2015-05-07 14:00:44 -0400'
categories:
- Uncategorized
tags: []
---
<blockquote>Responsive Email ResourcesA collection of tools &amp; resources for responsive email design</blockquote></p>
<p>Source: <em><a href="http://responsiveemailresources.com/">Responsive Email Resources</a></em></p>
