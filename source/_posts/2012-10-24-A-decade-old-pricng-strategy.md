---
layout: post
title: 'A Decade Old Pricing Strategy'
category: quote
tags: ['apple', 'economics', 'iPad', 'price', 'strategy']
---

> A decade-old pricing strategy

Apple is playing the strategy that they are best at, being the premium player in a market.

via -- [The iPad mini’s price is high, low, and everything in between - The Verge](http://www.theverge.com/2012/10/23/3544788/apple-ipad-mini-pricing-lower-cost-tablet)