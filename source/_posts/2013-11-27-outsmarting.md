---
layout: link
title: 'Outsmarting'
link: 'http://www.theverge.com/2012/11/27/3692738/ori-allon-urban-compass-twitter-google-goldman-sachs'
category: link
tags: ['economics', 'startups', 'technoogy', 'web']
---

> Allon was looking at the dominant paradigm on the web — by this point social, rather than search — and taking a highly technical approach to solving big pain points in that industry.

All it takes is outsmarting the big guys.

via -- [After cashing out to Google and Twitter, Ori Allon is going all in with Urban Compass - The Verge](http://www.theverge.com/2012/11/27/3692738/ori-allon-urban-compass-twitter-google-goldman-sachs)