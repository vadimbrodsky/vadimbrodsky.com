---
layout: post
title: 'Focus on focus'
category: quote
tags: ['focus', 'inspiration', 'workflow']
---

> Our ability to resist an impulse determines our success in learning a new behavior or changing an old habit. It’s probably the single most important skill for our growth and development.

The ability to focus is essential, especially in the high frequency information bombardment we have today. This is a great article that motivated me to try to start meditating.

via -- [If You’re Too Busy to Meditate, Read This - Harvard Business Review](http://blogs.hbr.org/2012/10/if-youre-too-busy-to-meditate/)