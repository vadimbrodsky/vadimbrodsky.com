---
layout: link
title: 'Git is cheap!'
link: 'https://coderwall.com/p/wxowig'
category: link
tags: ['development', 'git', 'workflow']
---

Git is awesome, it allows greater level of experimentation and faster development. Which reminds me that I need to learn some of the deeper features of Git, beyond simple committing and resetting.

via -- [coderwall.com](https://coderwall.com/p/wxowig)