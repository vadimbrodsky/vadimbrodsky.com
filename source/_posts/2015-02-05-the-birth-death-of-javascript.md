---
layout: post
status: publish
published: true
title: The Birth & Death of JavaScript
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 68
wordpress_url: http://www.vadimbrodsky.com/?p=68
date: '2015-02-05 14:34:46 -0500'
date_gmt: '2015-02-05 19:34:46 -0500'
categories:
- JavaScript
tags:
- technology
- web development
- conference talk
- future
---
<p>A highly entertaining talk by Gary Bernhardt from PyCon 2014 discussion the past and the potential future of JavaScript as the bytecode of the web.</p>
<blockquote><p>
  This science fiction / comedy / absurdist / completely serious talk traces the history of JavaScript, and programming in general, from 1995 until 2035. It's not pro- or anti-JavaScript; the language's flaws are discussed frankly, but its ultimate impact on the industry is tremendously positive.<br />
</blockquote></p>
<p>via -- <a href="https://www.destroyallsoftware.com/talks/the-birth-and-death-of-javascript">Destroy All Software</a></p>
