---
layout: page
title: About
permalink: /about/
---

<img src="/public/images/vadim-portrait.png" alt="Vadim Brodsky Portrait">

Vadim Brodsky is a designer, developer and technology enthusiast. He currently lives in Kitchener-Waterloo. During the day he spends his time as the Principal, Art & Technology at <a href="http://www.crankworks.ca" target="_blank">Crankworks Creative</a>.
