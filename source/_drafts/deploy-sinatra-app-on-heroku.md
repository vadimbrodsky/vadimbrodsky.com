---
layout: post
title: Deploy Sinatra App on Heroku
subhead: But it works on my machine!
---

Deploying your app from development to staging / production can give you important insight on the way your app will behave outside of the safe haven of your computer.

> Ship Early Ship Often, wait – was it Release early Release often?

Me

Recently I have been working on a Sinatra based BlackJack game as part of the TeaLeaf Academy Ruby Course.


### References
- [Deploying Rack-based Apps](https://devcenter.heroku.com/articles/rack#sinatra)
- [Getting Started with Ruby on Heroku](https://devcenter.heroku.com/articles/getting-started-with-ruby-o)
- [Get Started with Sinatra on Heroku](http://www.sitepoint.com/get-started-with-sinatra-on-heroku/)
