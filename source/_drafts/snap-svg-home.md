---
layout: post
status: draft
title: Snap.svg - Home
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 118
wordpress_url: http://www.vadimbrodsky.com/?p=118
date: '2015-02-14 13:31:44 -0500'
categories:
- Uncategorized
tags: []
---
<p>Why SVG (and Snap)?</p>
<p>SVG is an excellent way to create interactive, resolution-independent vector graphics that will look great on any size screen. And the Snap.svg JavaScript library makes working with your SVG assets as easy as jQuery makes working with the DOM.</p>
<p>via <a href='http://snapsvg.io/'>Snap.svg - Home</a>.</p>
