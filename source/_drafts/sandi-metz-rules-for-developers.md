---
layout: post
status: draft
title: Sandi Metz' Rules For Developers
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 169
wordpress_url: http://www.vadimbrodsky.com/?p=169
date: '2015-09-03 18:13:13 -0400'
categories:
- Uncategorized
- Post
tags: []
---
<blockquote>1. Classes can be no longer than one hundred lines of code.<br />
2. Methods can be no longer than five lines of code.<br />
3. Pass no more than four parameters into a method. Hash options are parameters.<br />
4. Controllers can instantiate only one object. Therefore, views can only know about one instance variable and views should only send messages to that object (@object.collaborator.value is not allowed).</blockquote></p>
<p>Source: <em><a href="https://robots.thoughtbot.com/sandi-metz-rules-for-developers">Sandi Metz' Rules For Developers</a></em></p>
