---
layout: post
status: draft
title: BrowserSync - Time-saving synchronised browser testing
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 120
wordpress_url: http://www.vadimbrodsky.com/?p=120
date: '2015-02-17 20:34:35 -0500'
categories:
- Uncategorized
tags: []
---
<p>Time-saving synchronised browser testing.</p>
<p>BrowserSync makes your tweaking and testing faster by synchronising file changes and interactions across multiple devices. It&rsquo;s wicked-fast and totally free.</p>
<p>via <a href='http://www.browsersync.io/'>BrowserSync - Time-saving synchronised browser testing</a>.</p>
