---
layout: post
status: draft
title: Funtools &ndash; Tools for Functional Ruby Style
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 209
wordpress_url: http://www.vadimbrodsky.com/?p=209
date: '2015-09-22 16:32:48 -0400'
categories:
- Uncategorized
tags: []
---
<p><a href="https://gitlab.com/wuest/funtools">Funtools</a> &ndash; Funtools is a collection of tools to help rubyists write ruby in a more functional style.</p>
