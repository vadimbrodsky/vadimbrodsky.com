---
layout: post
status: draft
title: jamesshore/quixote
author:
  display_name: Vadim
  login: vadim
  email: vadim@vadimbrodsky.com
  url: ''
author_login: vadim
author_email: vadim@vadimbrodsky.com
wordpress_id: 113
wordpress_url: http://www.vadimbrodsky.com/?p=113
date: '2015-02-13 09:12:02 -0500'
categories:
- Uncategorized
tags: []
---
<p>Quixote is a library for unit testing CSS. It lets you make assertions about your pages' elements, their relationships, and how they are actually styled by the browser. It has a compact, powerful API and produces beautiful failure messages.</p>
<p>via <a href='https://github.com/jamesshore/quixote'>jamesshore/quixote</a>.</p>
